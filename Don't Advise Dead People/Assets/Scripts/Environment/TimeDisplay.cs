using System;
using TMPro;
using UnityEngine;

namespace Platform.Environment
{
    public class TimeDisplay : MonoBehaviour
    {
        [SerializeField] private float timeToPlay;
        private float initialTimeToPlay;
        private LightingManager lightingManager = null;
        private bool runningGame = false;

        // Start is called before the first frame update
        void Start()
        {
            lightingManager = FindObjectOfType<LightingManager>();
            initialTimeToPlay = timeToPlay;
        }

        // Update is called once per frame
        void Update()
        {
            if (runningGame)
            {
                timeToPlay -= lightingManager.TimeSpeed();
            }
            if (timeToPlay > 0)
            {
                GetComponent<TextMeshProUGUI>().text = String.Format("{0:0}", timeToPlay);
            }
            else
            {
                GetComponent<TextMeshProUGUI>().text = String.Format("{0:0}", 0);
                if (runningGame)
                {
                    StartGame(false);
                }
            }
        }

        public void StartGame(bool start)
        {
            runningGame = start;
            timeToPlay = initialTimeToPlay;
        }

        public bool GetRunningGame()
        {
            return runningGame;
        }
    }
}