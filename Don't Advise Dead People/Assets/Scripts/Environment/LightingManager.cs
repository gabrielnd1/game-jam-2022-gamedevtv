using UnityEngine;

namespace Platform.Environment
{
    [ExecuteAlways]
    public class LightingManager : MonoBehaviour
    {
        //Scene References
        [SerializeField] private Light directionalLight;

        [SerializeField] private LightingPreset preset;
        
        [SerializeField] private float daySpeed = 0.1f;
        private float currentDaySpeed;

        //Variables
        [SerializeField, Range(0, 24)] private float timeOfDay;
        private float initialTimeOfDay;

        private bool isPlayerDead = false;
        private bool revengeTime = false;

        private void Start()
        {
            currentDaySpeed = daySpeed * 5;
            initialTimeOfDay = timeOfDay;
        }


        private void Update()
        {
            if (preset == null)
                return;

            if (Application.isPlaying)
            {
                timeOfDay += Time.deltaTime*currentDaySpeed;
                timeOfDay %= 24; //Modulus to ensure always between 0-24
                UpdateLighting(timeOfDay / 24f);
            }
            else
            {
                UpdateLighting(timeOfDay / 24f);
            }

            if (timeOfDay > 23 && isPlayerDead)
            {
                SummonTheDead();
                revengeTime = true;
            }
        }


        private void UpdateLighting(float timePercent)
        {
            //Set ambient and fog
            RenderSettings.ambientLight = preset.ambientColor.Evaluate(timePercent);
            RenderSettings.fogColor = preset.fogColor.Evaluate(timePercent);

            //If the directional light is set then rotate and set it's color, I actually rarely use the rotation because it casts tall shadows unless you clamp the value
            if (directionalLight != null)
            {
                directionalLight.color = preset.directionalColor.Evaluate(timePercent);

                directionalLight.transform.localRotation =
                    Quaternion.Euler(new Vector3((timePercent * 360f) - 90f, 170f, 0));
            }

        }

        //Try to find a directional light to use if we haven't set one
        private void OnValidate()
        {
            if (directionalLight != null)
                return;

            //Search for lighting tab sun
            if (RenderSettings.sun != null)
            {
                directionalLight = RenderSettings.sun;
            }
            //Search scene for light that fits criteria (directional)
            else
            {
                Light[] lights = GameObject.FindObjectsOfType<Light>();
                foreach (Light light in lights)
                {
                    if (light.type == LightType.Directional)
                    {
                        directionalLight = light;
                        return;
                    }
                }
            }
        }

        public void DeathIsOn()
        {
            isPlayerDead = true;
            currentDaySpeed = 1f;
        }

        private void SummonTheDead()
        {
            currentDaySpeed = daySpeed;
        }

        public bool GetRevenge()
        {
            return revengeTime;
        }

        public float TimeSpeed()
        {
            return Time.deltaTime * currentDaySpeed;
        }

        public void GameIsPlaying()
        {
            timeOfDay = initialTimeOfDay;
            currentDaySpeed = daySpeed;
        }
    }
}