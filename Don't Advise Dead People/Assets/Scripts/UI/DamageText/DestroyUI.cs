using UnityEngine;

namespace Platform.UI.DamageText
{
    public class DestroyUI : MonoBehaviour
    {
        [SerializeField] private GameObject targetToDestroy = null;

        public void DestroyTarget()
        {
            Destroy(targetToDestroy);
        }
    }
}