using System;
using UnityEngine;
using UnityEngine.UI;

namespace Platform.UI.DamageText
{
    public class DamageText : MonoBehaviour
    {
        [SerializeField] private Text damageText = null;
        public void SetValue(float amount)
        {
            damageText.text = String.Format("{0:0}", amount);
        }
    }
}
