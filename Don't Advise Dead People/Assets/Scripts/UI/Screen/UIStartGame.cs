using Cinemachine;
using Platform.Environment;
using Platform.Saving;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Platform.UI.Screen
{
    public class UIStartGame : MonoBehaviour
    {
        [SerializeField] private Canvas startCanvas;
        [SerializeField] private CinemachineVirtualCamera cinemachineVirtualCamera;
        [SerializeField] private TimeDisplay timeDisplay;
        private SavingWrapper savingWrapper;
        private LightingManager lightingManager;
        public bool firstStart = true;
        
        private void Start()
        {
            lightingManager = FindObjectOfType<LightingManager>();
            
            savingWrapper = FindObjectOfType<SavingWrapper>();
        }

        private void Update()
        {
            if (!timeDisplay.GetRunningGame() && !startCanvas.enabled)
            {
                GameIsOff();
            }
        }

        public void GameIsOn()
        {
            if (firstStart)
            {
                lightingManager.GameIsPlaying();
                timeDisplay.StartGame(true);
                cinemachineVirtualCamera.Priority = 0;
                firstStart = false;
            }
            else
            {
                Scene scene = SceneManager.GetActiveScene(); 
                SceneManager.LoadScene(scene.name);
            }
        }
        
        public void GameIsOff()
        {
            timeDisplay.StartGame(false);
            startCanvas.enabled = true;
            cinemachineVirtualCamera.Priority = 500;
            startCanvas.enabled = true;
            FindObjectOfType<SavingWrapper>().Save();
            FindObjectOfType<SavingWrapper>().Load();
        }
    }
}