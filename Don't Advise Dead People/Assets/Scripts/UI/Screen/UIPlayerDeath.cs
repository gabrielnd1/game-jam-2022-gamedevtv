using System.Collections;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Platform.UI.Screen
{
    public class UIPlayerDeath : MonoBehaviour
    {
        //For now, using it if boss died too. Can modify if necessary.
        [SerializeField] private float delayTime = 2f;
        [SerializeField] private float fadeInTime = 3f;
        [SerializeField] private float fadeOutTime = 10f;
        [SerializeField] private TextMeshProUGUI[] deathMessage;
        private CanvasGroup canvasGroup;
        private Coroutine currentActiveFade = null;
        private bool finished = false;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0;
        }

        private void Start()
        {
            deathMessage = GetComponentsInChildren<TextMeshProUGUI>();
            int rand = Random.Range(0, deathMessage.Length);
            deathMessage[rand].enabled = true;
        }

        private void Update()
        {
            if (finished)
            {
                Wrapper();
            }
        }


        private void FadeOutImmediate()
        {
            if (canvasGroup != null)
                canvasGroup.alpha = 1;
        }

        private void FadeInImmediate()
        {
            if (canvasGroup != null)
                canvasGroup.alpha = 0;
        }

        private Coroutine FadeIn(float time)
        {
            return Fade(1, time);
        }

        private Coroutine FadeOut(float time)
        {
            return Fade(0, time);
        }

        private IEnumerator FadeRoutine(float target, float time)
        {
            new WaitForSeconds(delayTime);
            while (!Mathf.Approximately(canvasGroup.alpha, target))
            {
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, target, Time.deltaTime / time);
                yield return null;
            }

            finished = !finished;

        }

        private Coroutine Fade(float target, float time)
        {
            if (currentActiveFade != null)
            {
                StopCoroutine(currentActiveFade);
            }

            currentActiveFade = StartCoroutine(FadeRoutine(target, time));
            return currentActiveFade;
        }

        public void Wrapper()
        {
            if (finished)
            {
                FadeOut(fadeInTime);
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;
            }
            else
            {
                FadeIn(fadeOutTime);
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            }
        }
    }
}