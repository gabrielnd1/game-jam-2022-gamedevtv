﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

namespace Platform.Object
{


    public class Hideable : MonoBehaviour
    {
        private const float transparency = 0.5f;

        private Material[] materials;
        private bool hidden = false;

        private void OnEnable()
        {
            materials = GetComponent<MeshRenderer>().materials;
            foreach (var material in materials)
            {
                MakeMaterialTransparent(material);
            }
        }


        private void MakeMaterialTransparent(Material materialToSet)
        {
            materialToSet.SetFloat("_Mode", hidden ? 3 : 0);
            materialToSet.SetInt("_SrcBlend", (int) BlendMode.One);
            materialToSet.SetInt("_DstBlend", (int) BlendMode.OneMinusSrcAlpha);
            materialToSet.SetInt("_ZWrite", 0);
            materialToSet.DisableKeyword("_ALPHATEST_ON");
            materialToSet.DisableKeyword("_ALPHABLEND_ON");
            materialToSet.EnableKeyword("_ALPHAPREMULTIPLY_ON");
            materialToSet.renderQueue = 3000;

            Color color = materialToSet.color;
            color.a = transparency;
            materialToSet.color = color;
        }
    }
}