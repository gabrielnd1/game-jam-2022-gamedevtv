﻿using UnityEngine;

namespace Platform.Object
{
    public class RotateOscilateObjects : MonoBehaviour
    {

        //rotate in Z axis, oscilate in Y
        [SerializeField] private float rotateSpeed = 0.5f;
        [SerializeField] private float oscillationSpeed = 1f;
        [SerializeField] private float maxOscillationDistance = 0.5f;
        private bool isGoingUp = true;
        private float localPositionX;
        private float localPositionY;
        [SerializeField] private float initialAltitude;
        private float localPositionZ;
        private float adjustSpeed = 1f;
        [SerializeField] private bool zRotation = true;


        // Start is called before the first frame update
        void Start()
        {
            localPositionX = transform.localPosition.x;
            localPositionZ = transform.localPosition.z;
        }

        // Update is called once per frame
        void Update()
        {
            localPositionY = transform.localPosition.y;
            adjustSpeed =
                Mathf.Cos(Mathf.Abs(((initialAltitude - localPositionY) / maxOscillationDistance)) * Mathf.PI / 2) +
                0.1f;
            if (adjustSpeed < 0)
            {
                adjustSpeed = 1f;
            }
        }

        private void LateUpdate()
        {
            if (zRotation)
            {
                transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
            }
            else
            {
                transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
            }

            if (isGoingUp)
            {
                transform.localPosition = new Vector3(localPositionX,
                    localPositionY + oscillationSpeed * adjustSpeed * Time.deltaTime, localPositionZ);
                if (transform.localPosition.y > initialAltitude + maxOscillationDistance)
                {
                    isGoingUp = false;
                }
            }
            else
            {
                transform.localPosition = new Vector3(localPositionX,
                    localPositionY - oscillationSpeed * adjustSpeed * Time.deltaTime, localPositionZ);
                if (transform.localPosition.y < initialAltitude - maxOscillationDistance)
                {
                    isGoingUp = true;
                }
            }
        }
    }
}