
using Cinemachine;
using UnityEngine;

namespace Platform.Core
{
    public class CinemachineConfig : MonoBehaviour
    {
        private CinemachineFreeLook freeLook;

        void Awake()
        {
            freeLook = GetComponent<CinemachineFreeLook>();


            freeLook.m_YAxis.m_InputAxisName = "Mouse ScrollWheel";
            freeLook.m_XAxis.m_InputAxisName = "Mouse X";

        }


    }
}