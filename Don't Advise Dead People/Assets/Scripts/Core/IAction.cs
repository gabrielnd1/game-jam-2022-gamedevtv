namespace Platform.Core

{
    public interface IAction
    {
        void Cancel();
    }
}

