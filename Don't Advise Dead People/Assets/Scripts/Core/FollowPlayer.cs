using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platform.Core
{
    public class FollowPlayer : MonoBehaviour
    {
        private GameObject player;

        [SerializeField] private float distance = 0.01f;
        [SerializeField] private float tolerance = 0.01f;

        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindWithTag("Player");
        }

        void LateUpdate()
        {
            if (Mathf.Abs(Vector3.Distance(transform.parent.position, player.transform.position)
                          - distance) > tolerance)
            {
                transform.localPosition = new Vector3
                (0, 0, Vector3.Distance(transform.parent.position, player.transform.position)
                       - distance);
            }

        }
    }
}