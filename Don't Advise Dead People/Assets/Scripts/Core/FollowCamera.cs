using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platform.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] private bool IsRotating = false;
        [SerializeField] private float delayTime = 2f;
        private Vector3 newPosition;

        private int positionsQuantity;
        private List<Vector3> storedPositions = new List<Vector3>();

        // Update is called once per frame
        private void Update()
        {
            positionsQuantity = Mathf.RoundToInt(delayTime / Time.deltaTime);
            storedPositions.Add(target.transform.position);
            if (positionsQuantity < storedPositions.Count)
            {
                storedPositions.RemoveAt(0);
            }
        }

        void LateUpdate()
        {
            newPosition = storedPositions[Mathf.RoundToInt(storedPositions.Count/2)];
            newPosition.y = transform.position.y;
            transform.position = newPosition;

            if (IsRotating)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, target.rotation.eulerAngles.y, 0));
            }

        }
    }

}