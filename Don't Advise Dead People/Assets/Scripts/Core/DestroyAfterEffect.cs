using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Platform.Core
{


    public class DestroyAfterEffect : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if (GetComponentInChildren<ParticleSystem>() == null)
            {
                Destroy(gameObject);
                
            }
        }
    }
}
