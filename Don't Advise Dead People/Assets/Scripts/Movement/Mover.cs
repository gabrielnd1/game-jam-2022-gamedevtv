using System.Collections;
using System.Collections.Generic;
using Platform.Attributes;
using Platform.Core;
using UnityEngine;
using UnityEngine.AI;

namespace Platform.Movement
{
    public class Mover : MonoBehaviour, IAction
    {
        [SerializeField] private float maxSpeed = 5f;
        [SerializeField] private float maxNavPathLenght = 40f;

        private NavMeshAgent navMeshAgent;
        private Health health;

        private void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            health = GetComponentInChildren<Health>();
        }

        // Update is called once per frame
        void Update()
        {
            navMeshAgent.enabled = !health.IsDead();

        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            MoveTo(destination, speedFraction);
        }

        public bool CanMoveTo(Vector3 destination)
        {
            NavMeshPath path = new NavMeshPath();
            bool hasPath = NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path);
            if (!hasPath)
            {
                return false;

            }

            if (path.status != NavMeshPathStatus.PathComplete)
            {
                return false;
            }

            if (GetPathLenght(path) > maxNavPathLenght)
            {
                return false;

            }

            return true;
        }

        private float GetPathLenght(NavMeshPath path)
        {
            float total = 0;
            if (path.corners.Length < 2) return total;
            for (int i = 0; i < path.corners.Length - 1; i++)
            {
                total += Vector3.Distance(path.corners[i], path.corners[i + 1]);
            }

            return total;
        }

        public void MoveTo(Vector3 destination, float speedFraction)
        {
            navMeshAgent.destination = destination;
            navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
            navMeshAgent.isStopped = false;
        }

        public void RotateTo(bool right, float speedFraction)
        {
            Vector3 direction;
            if (right)
            {
                direction = transform.right * maxSpeed/speedFraction + transform.forward * maxSpeed/speedFraction;
            }
            else
            {
                direction = -transform.right * maxSpeed/speedFraction + transform.forward * maxSpeed/speedFraction;
            }
            MoveTo(transform.position+direction,speedFraction);
            Debug.DrawLine(transform.position,(transform.position+direction)*100,Color.red);
        }

        public void Cancel()
        {
            if (navMeshAgent != null)
            {
                navMeshAgent.isStopped = true;
            }
        }

        public void MultiplyMaxSpeed(float multiple)
        {
            maxSpeed = multiple * maxSpeed;
        }


    }

}