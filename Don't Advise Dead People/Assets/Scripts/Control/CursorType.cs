namespace Platform.Control
{
    public enum CursorType
    {
        None,
        Movement,
        Combat,
        UI
    }
}