namespace Platform.Control
{
//use it to pick up objects
    public interface IRaycastable
    {
        CursorType GetCursorType();
        bool HandleRaycast(PlayerController callingController);
        ShowOutline HasOutline(PlayerController callingController);
    }
}
