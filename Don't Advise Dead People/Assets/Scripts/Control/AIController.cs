using Platform.Attributes;
using Platform.Combat;
using Platform.Core;
using Platform.Movement;
using UnityEngine;

namespace Platform.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] private float chaseDistance = 5f;
        [SerializeField] private float suspicionTime = 3f;
        public PatrolPath patrolPath;
        [SerializeField] private float waypointTolerance = 1f;
        [SerializeField] private float waypointDwellTime = 3f;
        [Range(0,1)]
        [SerializeField] private float patrolSpeedFraction = 1f;
        [SerializeField] private float aggroCooldownTime = 5f;
        [SerializeField] private float shoutDistance = 5f;
        private GameObject player;
        private Fighter fighter;
        private float distanceToPlayer;
        private Health health;
        private Mover mover;
        private RaycastHit[] hits;
        private RaycastHit[] blockers;
        private bool haveVision = false;

        Vector3 guardPosition;
        private float timeSinceLastSawPlayer = Mathf.Infinity;
        private float timeSinceArrivedAtWaypoint = Mathf.Infinity;
        private float timeSinceAggrevated = Mathf.Infinity;
        private int currentWaypointIndex = 0;

        private int randomChanceToSpecial;

    
        private void Awake()
        {
            player = GameObject.FindWithTag("Player");
            fighter = GetComponent<Fighter>();
            health = GetComponentInChildren<Health>();
            mover = GetComponent<Mover>();

        }

        private Vector3 GetGuardPosition()
        {
            return transform.position;
        }

        private void Update()
        {
            if(health.IsDead()){return;}
            if (IsAggrevated() && fighter.CanAttack(player))
            {
                AttackBehaviour();
            }
            else if (timeSinceLastSawPlayer < suspicionTime)
            {
                SuspicionBehaviour();
            }
            else
            {
                PatrolBehaviour();
            }

            UpdateTimers();
        }
        public void Aggrevate()
        {
            if (player.GetComponent<Health>().IsDead())
            {
                timeSinceAggrevated = Mathf.Infinity;
            }
            timeSinceAggrevated = 0;
        }

        private void UpdateTimers()
        {
            timeSinceLastSawPlayer += Time.deltaTime;
            timeSinceArrivedAtWaypoint += Time.deltaTime;
            timeSinceAggrevated += Time.deltaTime;
            
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPosition = guardPosition;

            if (patrolPath != null)
            {
                if (AtWaypoint())
                {
                    timeSinceArrivedAtWaypoint = 0;
                    CycleWaypoint();
                }

                nextPosition = GetCurrentWaypoint();
            
                if (timeSinceArrivedAtWaypoint > waypointDwellTime)
                {
                    mover.StartMoveAction(nextPosition, patrolSpeedFraction);
                }
            
            }
            else
            {
                mover.StartMoveAction(guardPosition, patrolSpeedFraction);
            }

        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetWaypoint(currentWaypointIndex);
        }

        private void CycleWaypoint()
        {
            currentWaypointIndex = patrolPath.GetNextIndex(currentWaypointIndex);
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void SuspicionBehaviour()
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AttackBehaviour()
        {
            if (player.GetComponent<Health>().IsDead())
            {
                return;
            }
            if (distanceToPlayer < chaseDistance)
            {
                timeSinceLastSawPlayer = 0;
            }

            AggrevateNearbyEnemies();

                
            fighter.Attack(player);
        }


        private void AggrevateNearbyEnemies()
        {
            if (player.GetComponent<Health>().IsDead())
            {
                return;
            }
            hits = Physics.SphereCastAll(transform.position, shoutDistance, Vector3.up, 0);
            foreach (RaycastHit hit in hits)
            {
                AIController ai = hit.collider.GetComponent<AIController>();
                if(ai == null) continue;
                if(DoesHaveVision(gameObject, ai.gameObject))
                {
                    if (ai != this)
                    {
                        ai.Aggrevate();
                    }
                }
            }
        }

        private bool IsAggrevated()
        {
            if (player.GetComponent<Health>().IsDead())
            {
                return false;
            }
            distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
            if (distanceToPlayer < chaseDistance)
            {
                haveVision = DoesHaveVision(gameObject, player);
            }
            else
            {
                haveVision = false;
            }
            return haveVision || timeSinceAggrevated < aggroCooldownTime;
        }

        //Called by Unity
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }

        private bool DoesHaveVision(GameObject object1, GameObject object2)
        {
            if (player.GetComponent<Health>().IsDead())
            {
                return false;
            }
            LayerMask wall = LayerMask.GetMask("Wall");
            LayerMask mask = wall;
            float consideredDistance = Mathf.Min(Vector3.Distance(object1.transform.position, object2.transform.position),
                chaseDistance);
            
            blockers = Physics.RaycastAll(object1.GetComponent<Fighter>().GetHeadPosition(), 
                (object2.GetComponent<Fighter>().GetHeadPosition()-object1.GetComponent<Fighter>().GetHeadPosition()).normalized, 
                consideredDistance, mask);
            

             if (blockers.Length > 0)
             {
                 return false;
             }

            return true;
        }
        
        private void DeactivateMovement()
        {
            mover.Cancel();
            fighter.Cancel();
            mover.MoveTo(transform.position,0);
            mover.enabled = false;
            fighter.enabled = false;
        }

        private void ActivateMovement()
        {
            mover.enabled = true;
            fighter.enabled = true;
        }
    }
}
    
