using UnityEngine;

namespace Platform.Control
{

    public class ShowOutline : MonoBehaviour
    {
        private cakeslice.Outline[] outlines;

        private void Start()
        {
            outlines = GetComponentsInChildren<cakeslice.Outline>();
        }

        public void MakeOutline()
        {
            foreach (var outline in outlines)
            {
                outline.eraseRenderer = false;
            }
        }

        public void EraseOutline()
        {
            foreach (var outline in outlines)
            {
                outline.eraseRenderer = true;
            }
        }
    }
}