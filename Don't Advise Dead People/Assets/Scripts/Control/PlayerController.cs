using System;
using Platform.Movement;
using Platform.Attributes;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Cursor = UnityEngine.Cursor;

namespace Platform.Control //remember isdraggingUI in CameraController
{
    public class PlayerController : MonoBehaviour
    {
        private Health health;

        
        
        [System.Serializable]
        struct CursorMapping
        {
            public CursorType type;
            public Texture2D texture;
            public Vector2 hotspot;
        }

        [SerializeField] private CursorMapping[] cursorMappings = null;
        [SerializeField] private float maxNavMeshProjectionDistance = 1f;


        private float timePassed;

        private ShowOutline lastOutline = null;

        private void Start()
        {
            health = GetComponentInChildren<Health>();
        }

        // Update is called once per frame
        void Update()
        {
            if (InteractWithUI())
            {
                return;
            }
            if (health.IsDead())
            {
                SetCursor(CursorType.None);
                return;
            }

            if (InteractWithComponent()) return;
            
            if (InteractWithMovement()) return;
                SetCursor(CursorType.None);
                
            
        }

        private bool InteractWithUI()
        {
            if (lastOutline != null)
            {
                lastOutline.EraseOutline();
                lastOutline = null;
            }
            if (EventSystem.current.IsPointerOverGameObject())
            {
                SetCursor(CursorType.UI);
                return true;
            }

            return false;
        }

        private bool InteractWithComponent()
        {
            RaycastHit[] hits = RaycastAllSorted();
            foreach (RaycastHit hit in hits)
            {
                IRaycastable[] raycastables = hit.transform.GetComponents<IRaycastable>();
                foreach (IRaycastable raycastable in raycastables)
                {
                    if (raycastable.HandleRaycast(this))
                    {
                        SetCursor(raycastable.GetCursorType());

                        if (raycastable.HasOutline(this) != null && raycastable.HasOutline(this) != lastOutline)
                        {
                            if (lastOutline != null)
                            {
                                lastOutline.EraseOutline();
                            }
                            lastOutline = raycastable.HasOutline(this);
                            lastOutline.MakeOutline();
                        }
                        
                        return true;
                    }
                }
            }

            return false;
        }

        RaycastHit[] RaycastAllSorted()
        {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            float[] distances = new float[hits.Length];

            for (int i = 0; i < hits.Length; i++)
            {
                distances[i] = hits[i].distance;

            }
            Array.Sort(distances, hits);
            return hits;
        }
        
        private bool InteractWithMovement()
        {
            
            if (lastOutline != null)
            {
                lastOutline.EraseOutline();
                lastOutline = null;
            }
            Vector3 target;
            //int layerMask = ~ 1 << 9; not being used. but concept is here.
            bool hasHit = RaycastNavMesh(out target);
            if (hasHit)
            {
                if (!GetComponent<Mover>().CanMoveTo(target))
                {
                    return false;
                }

                if (Input.GetMouseButton(0))
                {
                    GetComponent<Mover>().StartMoveAction(target, 1f);
                    /*if (!isMoveEffectOn)
                    {
                        GameObject pointer= Instantiate(moveEffect, target, Quaternion.Euler(90, 0, 0));
                        print("pointer " + pointer);
                        isMoveEffectOn = true;
                        timePassed = 0f;
                    }*/
                }

                SetCursor(CursorType.Movement);

                
                return true;
            }

            return false;
        }

        private bool RaycastNavMesh(out Vector3 target)
        {
            target = new Vector3();
            RaycastHit[] hits;
            RaycastHit hit;
            hits = Physics.RaycastAll(GetMouseRay());
            foreach (var hitResult in hits)
            {
                if (hitResult.transform.gameObject.layer == 3)
                {
                    
                    hit = hitResult;
                    target = hitResult.point;
                    break;
                }
            }

            NavMeshHit navMeshHit;
            bool hasCastToNavMesh = NavMesh.SamplePosition(
                target, out navMeshHit, maxNavMeshProjectionDistance, NavMesh.AllAreas);
            if (!hasCastToNavMesh) return false;
            target = navMeshHit.position;

          
            return true;
        }


        private static Ray GetMouseRay() 
        {
                return Camera.main.ScreenPointToRay(Input.mousePosition);
        }

        public void SetCursor(CursorType type)
        {
            CursorMapping mapping = GetCursorMapping(type);
            Cursor.SetCursor(mapping.texture, mapping.hotspot, CursorMode.Auto);
        }

        private CursorMapping GetCursorMapping(CursorType type)
        {
            foreach (CursorMapping mapping in cursorMappings)
            {
                if (mapping.type == type)
                {
                    return mapping;
                }
            }

            return cursorMappings[0];
        }

        /*private void LateUpdate()
        {
            if (timePassed > timeBetweenEffects)
            {
                isMoveEffectOn = false;
                
            }
            else
            {
                timePassed += Time.deltaTime;
            }
        }*/
    }
}
