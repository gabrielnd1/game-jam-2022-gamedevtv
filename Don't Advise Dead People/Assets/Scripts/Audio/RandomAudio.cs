using UnityEngine;
using Random = UnityEngine.Random;

namespace Platform.Audio
{
    public class RandomAudio : MonoBehaviour
    {
        //VolumeControl script not present in this case
        
        [SerializeField] private AudioClip[] sfx;

        private float _volume;
        private float _pitch;
        private float initialVolume;
        [SerializeField] private float maxVolume = 1;
        [SerializeField] private float minVolume = 0;
        [SerializeField] private float maxPitch = 1.1f;
        [SerializeField] private float minPitch = 0.9f;
        private AudioSource _audioSource;
        [SerializeField] private bool playOnAwake = false;
        [SerializeField] private bool isThisMusic = false;

        private int _random;

        void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            
            _pitch = Random.Range(minPitch, maxPitch);
            _audioSource.pitch = _pitch;
            
            if (sfx.Length != 0)
            {
                _random = Random.Range(0, sfx.Length);
                _audioSource.clip = sfx[_random];
            }

            if (playOnAwake)
            {
                PlaySound();
            }
        }

        public void PlaySound()
        {
            initialVolume = Random.Range(minVolume, maxVolume);
            _audioSource.volume = initialVolume;
            _pitch = Random.Range(minPitch, maxPitch);
            _audioSource.pitch = _pitch;
            if (sfx.Length != 0)
            {
                _random = Random.Range(0, sfx.Length);
                _audioSource.clip = sfx[_random];
            }

            if (isThisMusic)
            {
                _volume = initialVolume;
                _audioSource.volume = _volume;
            }
            else
            {
                _volume = initialVolume;
                _audioSource.volume = _volume;
            }
            if (sfx.Length != 0)
            {
                _audioSource.clip = sfx[_random];
            }

            if (_audioSource.enabled)
            {
                _audioSource.Play();
            }
            

        }

        
    }
}
