using UnityEngine;

namespace Platform.Saving
{
    public class SaveCondition : MonoBehaviour
    {
        [SerializeField] private bool saveIfDisable = false;
        private const string defaultSaveFile = "save";

        private void OnDisable()
        {
            if (saveIfDisable)
            {
                FindObjectOfType<SavingSystem>().Save(defaultSaveFile);
            }
        }
    }
}