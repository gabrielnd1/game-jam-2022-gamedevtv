using UnityEngine;
using UnityEngine.Events;
using Platform.Core;
using Platform.Environment;
using Platform.Stats;

namespace Platform.Attributes
                   {
                       public class Health : MonoBehaviour //it is possible to put ISaveable to save health and IModifierProvider to put health by inventory items
                       {
                           [SerializeField] private float regenerationPercentage = 100f;
                           [SerializeField] private TakeDamageEvent takeDamage;
                           [SerializeField] private UnityEvent onDie;
                           [SerializeField] private bool dieTwice = false;
                           [SerializeField] private float initialHealthPoints = 100f;
                           [SerializeField] private int pointsToGain = 10;
                           [SerializeField] private GameObject deathAnimation = null;
                           private float healthPoints;
                           private bool isDead = false;
                           private bool isUndead = false; //TODO make enemies run. It will be unfinished, sadly
                           
                           [System.Serializable]
                           public class TakeDamageEvent : UnityEvent<float>
                           {
                               
                           }
                           
                   
                           private void Awake()
                           {
                               healthPoints = initialHealthPoints;
                           }
                   
                   
                           public bool IsDead()
                           {
                               return isDead;
                           }
                   
                           public void TakeDamage(GameObject Instigator, float damage)
                           {
                               healthPoints = Mathf.Max(healthPoints - damage,0);
                               if (healthPoints == 0)
                               {
                                   onDie.Invoke();
                                   Die();
                                   AwardPoints(Instigator);
                               }
                               else
                               {
                                   takeDamage.Invoke(damage);
                               }
                               
                           }
                           
                           public void Heal(float healthToRestore)
                           {
                               healthPoints = Mathf.Min(healthPoints + healthToRestore, GetMaxHealthPoints());
                           }
                   
                           public float GetHealthPoints()
                           {
                               return healthPoints;
                           }
                   
                           public float GetMaxHealthPoints()
                           {
                               return initialHealthPoints;
                           }
                           
                           public float GetPercentage()
                           {
                               return 100 * GetFraction();
                           }
                           
                           public float GetFraction()
                           {
                               return healthPoints / initialHealthPoints;
                           }
                   
                           private void Die()
                           {
                               transform.gameObject.tag = "Dead";
                               if(isDead) return;
                               if (dieTwice)
                               {
                                   FindObjectOfType<LightingManager>().DeathIsOn();
                               }
                               isDead = true;
                               
                               GetComponent<ActionScheduler>().CancelCurrentAction();
                   
                               GetComponent<BoxCollider>().enabled = false;


                               GameObject emptyObject = new GameObject();
                               GameObject dyingShip = Instantiate(emptyObject, transform);
                               dyingShip.name = "Dying Ship";
                               
                               deathAnimation.transform.parent = dyingShip.transform;
                               dyingShip.transform.parent = null;
                               deathAnimation.GetComponent<Animation>().Play();
                           }
                   
                           private void AwardPoints(GameObject instigator)
                           {
                               //TODO AwardPoints
                               Record record = instigator.GetComponent<Record>();
                               if (record == null) return;

                               record.GainPoints(pointsToGain);
                           }
                   
                           public void RiseAgain()
                           {
                               isUndead = true;
                               isDead = false;
                               transform.gameObject.tag = "GhostShip";
                               initialHealthPoints = initialHealthPoints * initialHealthPoints;
                               float regenHealthPoints = initialHealthPoints * (regenerationPercentage / 100);
                               healthPoints = Mathf.Max(healthPoints, regenHealthPoints);
                           }
                       } 
                   }
