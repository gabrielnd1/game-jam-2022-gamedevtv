using UnityEngine;

namespace Platform.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Health healthComponent = null;
        [SerializeField] private RectTransform foreground = null;
        [SerializeField] private Canvas rootCanvas;

        // Start is called before the first frame update
        void Start()
        {
            rootCanvas.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (Mathf.Approximately((healthComponent.GetFraction()), 0) ||
                Mathf.Approximately((healthComponent.GetFraction()), 1))
            {
                rootCanvas.enabled = false;
                return;
            }

            rootCanvas.enabled = true;
            foreground.localScale = new Vector3(healthComponent.GetFraction(), 1, 1);


        }
    }
}