using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Platform.Stats
{
    public class RecordDisplay : MonoBehaviour
    {
        Record record;
        [SerializeField] private bool isRecord = false;

        private void Awake()
        {
            record = GameObject.FindWithTag("Player").GetComponent<Record>();
        }

        private void Update()
        {
            if (isRecord)
            {
                GetComponent<TextMeshProUGUI>().text = String.Format("{0:0}", record.GetRecord());
            }
            else
            {
                GetComponent<TextMeshProUGUI>().text = String.Format("{0:0}", record.GetPoints());
            }
        }
    }
}