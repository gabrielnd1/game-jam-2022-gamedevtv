using System;
using System.Collections;
using System.Collections.Generic;
using Platform.Saving;
using UnityEngine;

namespace Platform.Stats
{


    public class Record : MonoBehaviour, ISaveable
    {
        private int oldRecord;
        [SerializeField] private int points = 0;
        
        public void GainPoints(int pointsGained)
        {
            points += pointsGained;
        }

        public int GetPoints()
        {
            return points;
        }

        public int GetRecord()
        {
            if (points>oldRecord)
            {
                return points;
            }

            return oldRecord;
        }

        public object CaptureState()
        {
            if (oldRecord < points)
            {
                return points;
            }

            return oldRecord;
        }

        public void RestoreState(object state)
        {
            oldRecord = (int) state;
        }
    }
}