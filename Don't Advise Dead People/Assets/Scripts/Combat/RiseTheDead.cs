using Cinemachine;
using Platform.Attributes;
using Platform.Environment;
using Platform.Movement;
using UnityEngine;

namespace Platform.Combat
{
    public class RiseTheDead : MonoBehaviour
    {
        private LightingManager lightManager;

        [SerializeField] private GameObject ghostShip = null;
        [SerializeField] private WeaponConfig ghostCannon = null;
        [SerializeField] private Transform portCannon = null;
        [SerializeField] private Transform starboardCannon = null;
        [SerializeField] private Transform weaponTransform = null;
        [SerializeField] private CinemachineVirtualCamera deathCamera = null;
        private bool isRisen = false;
        
        // Start is called before the first frame update
        void Start()
        {
            lightManager = FindObjectOfType<LightingManager>();

        }

        // Update is called once per frame
        void Update()
        {
            if (lightManager.GetRevenge() && !isRisen)
            {
                RevengeIsNight();
            }
        }

        private void RevengeIsNight()
        {
            ghostShip.SetActive(true);
            GetComponentInParent<Fighter>().enabled = true;
            GetComponentInParent<Fighter>().SetCannonTransform(starboardCannon,portCannon,weaponTransform);
            GetComponentInParent<Fighter>().EquipWeapon(ghostCannon);
            GetComponentInParent<Health>().RiseAgain();
            GetComponentInParent<Mover>().MultiplyMaxSpeed(1.2f);
            deathCamera.Priority = 0;
            isRisen = true;
        }

    }
}