using Platform.Attributes;
using UnityEngine;

namespace Platform.Combat
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Make New Weapon", order = 0)]
    public class WeaponConfig : ScriptableObject
    {
        [SerializeField] private float weaponRange = 1f;
        [SerializeField] private float weaponDamage = 1f;
        [SerializeField] private Weapon equippedPrefab = null;
        [SerializeField] private Projectile projectile = null;
        [SerializeField] private Quaternion adjustAngle = Quaternion.identity;

        private const string weaponName = "Weapon";

        public Weapon Spawn(Transform weaponTransform)
        {
            DestroyOldWeapon(weaponTransform);

            Weapon weapon = null;
            if (equippedPrefab != null)
            {
                Transform handTransform = weaponTransform;
                weapon = Instantiate(equippedPrefab, handTransform);
                weapon.gameObject.name = weaponName;
            }

            return weapon;
        }

        private void DestroyOldWeapon(Transform weaponTransform)
        {
            Transform oldWeapon = weaponTransform.Find(weaponName);

            if (oldWeapon == null) return;

            oldWeapon.name = "DESTROYING";
            Destroy(oldWeapon.gameObject);
        }

        public bool HasProjectile()
        {
            return projectile != null;
        }

        public void LaunchProjectile(Transform weaponTransform, Health target, GameObject instigator, float calculatedDamage)
        {
            Projectile projectileInstance =
                Instantiate(projectile, weaponTransform.position, Quaternion.identity*adjustAngle);
            projectileInstance.SetTarget(target, instigator, calculatedDamage);
        }
        
        public float GetDamage()
        {
            return weaponDamage;
        }
        
        public float GetRange()
        {
            return weaponRange;
        }
    }
}
