using System.Collections.Generic;
using Platform.Attributes;
using Platform.Core;
using Platform.Movement;
using UnityEngine;

namespace Platform.Combat
{
    
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] private float timeBetweenAttacks = 5f;
        private Transform closerCannonTransform = null;
        [SerializeField] private Transform portCannon = null;
        [SerializeField] private Transform starboardCannon = null;
        [SerializeField] private Transform weaponTransform = null;
        [SerializeField] private WeaponConfig defaultWeapon = null;
        [SerializeField] private float aimTime = 10f;


        private float timeSinceLastAttack = Mathf.Infinity;
        private float timeSinceChangedTarget = 0;
        private bool targetInAim = false;
        
        [SerializeField] private Health target;
        private bool inRange = false;
        private Vector3 LookingAt;
        private RaycastHit[] blockers;
        private RaycastHit[] possibleTargetsCenter;

        [SerializeField] private WeaponConfig currentWeaponConfig;
        private Weapon currentWeapon;

        private float damage = 10f;

        private void Awake()
        {
            currentWeaponConfig = defaultWeapon;
            currentWeapon = SetupDefaultWeapon();
            closerCannonTransform = transform;
        }

        private Weapon SetupDefaultWeapon()
        {
             
            return AttachWeapon(defaultWeapon);;
        }

        private void Update()
        {
            timeSinceLastAttack += Time.deltaTime;
            timeSinceChangedTarget += Time.deltaTime;
            

            if (target == null)
            {
                ResetAim();
                return;
            }
            if (target.IsDead())
            {
                ResetAim();
                return;
            }

            if (timeSinceChangedTarget>aimTime)
            {
                targetInAim = true;
            }
            
            if (!GetIsInRange(target.gameObject.GetComponent<Fighter>().transform) && !inRange)
            {
                GetComponent<Mover>().MoveTo(target.transform.position, 1f);
            }
            else if (!GetIsInShootingZone(target.gameObject.GetComponent<Fighter>().transform))
            {
                float dotProd = Vector3.Dot(target.transform.position, closerCannonTransform.position);
                GetComponent<Mover>().RotateTo(dotProd>0, 0.2f);

            }
            else

            {
                GetComponent<Mover>().Cancel();
                AttackBehavior();
            }
            
        }

        private void ResetAim()
        {
            timeSinceChangedTarget = 0;
            targetInAim = false;
        }

        public void EquipWeapon(WeaponConfig weaponConfig)
        {
            currentWeaponConfig = weaponConfig;
            if (weaponConfig == null)
            {
                return;
            }

            damage = weaponConfig.GetDamage();
            
            currentWeapon = AttachWeapon(weaponConfig);
        }

        private Weapon AttachWeapon(WeaponConfig weaponConfig)
        {
            if (weaponConfig == null)
            {
                return null;
            }

            damage = weaponConfig.GetDamage();
            return weaponConfig.Spawn(weaponTransform);
            
        }

        public Health GetTarget()
        {
            return target;
        }


        //This is to set the speed of attack and trigger animation
        private void AttackBehavior()
        {
            if (timeSinceLastAttack > timeBetweenAttacks && targetInAim)
            {
                Hit();
                timeSinceLastAttack = 0;
            }
        }

        private void Hit()
        {
            if(target == null) {return;}
            if(currentWeaponConfig == null) {return;}

            if (currentWeapon != null)
            {
                currentWeapon.OnHit();
            }

            if (currentWeaponConfig.HasProjectile())
            {
                currentWeaponConfig.LaunchProjectile(weaponTransform,target,gameObject, damage);
            }
            else
            {
                target.TakeDamage(gameObject, damage);
            }
            
        }

        private void Unload()
        {
            GetComponentInChildren<DisappearArrow>().Unload();
        }
        private void Reload()
        {
            GetComponentInChildren<DisappearArrow>().Reload();
        }
        private bool GetIsInRange(Transform targetTransform)
        {
            if (currentWeaponConfig == null) return true;

            if (Vector3.Distance(portCannon.position, targetTransform.position) < Vector3.Distance(starboardCannon.position, targetTransform.position))
            {
                closerCannonTransform = portCannon;
            }
            else
            {
                closerCannonTransform = starboardCannon;
            }
            
            return Vector3.Distance(closerCannonTransform.position, targetTransform.position) < currentWeaponConfig.GetRange()
                && DoesHaveVision(closerCannonTransform, targetTransform);
        }
        

        private bool GetIsInShootingZone(Transform targetTransform)
        {

            Transform[] origins = closerCannonTransform.GetComponentsInChildren<Transform>();
            
            possibleTargetsCenter = Physics.RaycastAll(closerCannonTransform.position,closerCannonTransform.forward*currentWeaponConfig.GetRange());

            foreach (var hit in possibleTargetsCenter)
            {
                if (hit.transform == targetTransform)
                {
                    Debug.DrawLine(closerCannonTransform.position,closerCannonTransform.forward*currentWeaponConfig.GetRange());
                    return true;
                }
            }

            foreach (var origin in origins)
            {
                possibleTargetsCenter = Physics.RaycastAll(origin.position,origin.forward*currentWeaponConfig.GetRange());
                foreach (var hit in possibleTargetsCenter)
                {
                    if (hit.transform == targetTransform)
                    {
                        Debug.DrawLine(origin.position,origin.forward*currentWeaponConfig.GetRange());
                        return true;
                    }
                }
            }
            
            return false;
        }

        public bool CanAttack(GameObject combatTarget)
        {
            if (combatTarget == null) { return false;}

            if (!GetComponent<Mover>().CanMoveTo(combatTarget.transform.position) &&
                !GetIsInRange(combatTarget.gameObject.GetComponent<Fighter>().closerCannonTransform))
            {
                return false;
            }
            Health targetToTest = combatTarget.GetComponent<Health>();
            return targetToTest != null && !targetToTest.IsDead();
        }

        public void Attack(GameObject combatTarget)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            if (target != combatTarget.GetComponent<Health>())
            {
                ResetAim();
                target = combatTarget.GetComponent<Health>();
            }
            if (target.GetComponent<CombatTarget>() != null)
            {
                target.GetComponent<CombatTarget>().IsTarget();
            }
        }
        public void Cancel()
        {
            StopAttack();
            if (target.GetComponent<CombatTarget>() != null)
            {
                target.GetComponent<CombatTarget>().IsNotTarget();
            }

            target = null;
        }

        private void StopAttack()
        {
            ResetAim();
        }
        
        private bool DoesHaveVision(Transform transform1, Transform transform2)
        {
            LayerMask wall = LayerMask.GetMask("Wall");
            LayerMask mask = wall;
            float consideredDistance = Mathf.Min(Vector3.Distance(transform1.position, transform2.position),
                currentWeaponConfig.GetRange());
            blockers = Physics.RaycastAll(transform1.position, (transform2.position-transform1.position).normalized, consideredDistance, mask);
            if (blockers.Length > 0)
            {
                return false;
            }
            return true;
        }

        public WeaponConfig GetDefaultWeapon()
        {
            return defaultWeapon;
        }

        public bool IsInRange()
        {
            return inRange;
        }

        public void InsideRange(bool inside)
        {
            inRange = inside;
        }

        public WeaponConfig GetCurrentWeaponConfig()
        {
            return currentWeaponConfig;
        }

        public Vector3 GetHeadPosition()
        {
            return closerCannonTransform.position;
        }

        public void SetCannonTransform(Transform newStarboard, Transform newPort, Transform newWeapon)
        {
            starboardCannon = newStarboard;
            portCannon = newPort;
            weaponTransform = newWeapon;
        }
    }
    

}
