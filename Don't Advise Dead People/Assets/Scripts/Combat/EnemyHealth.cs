using System;
using Platform.Combat;
using UnityEngine;
using UnityEngine.UI;

namespace Platform.Attributes
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        private Fighter fighter;
        private Health health;

        private void Start()
        {
        }

        private void Awake()
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
        }

        // Update is called once per frame
        void Update()
        {
            if (fighter.GetTarget() == null)
            {
                GetComponent<Text>().text = "N/A";
            }
            else
            {
                health = fighter.GetTarget();
                GetComponent<Text>().text = String.Format("{0:0}/{1:0}", health.GetHealthPoints(), health.GetMaxHealthPoints());
            }
        }
    }
}