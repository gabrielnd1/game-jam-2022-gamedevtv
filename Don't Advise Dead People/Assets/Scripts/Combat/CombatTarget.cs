using System.Collections;
using System.Collections.Generic;
using Platform.Attributes;
using Platform.Control;
using UnityEngine;

namespace Platform.Combat
{
    [RequireComponent(typeof(Health))]
    public class CombatTarget : MonoBehaviour, IRaycastable
    {
        [SerializeField] private GameObject token;
        public CursorType GetCursorType()
        {
            return CursorType.Combat;
        }

        public bool HandleRaycast(PlayerController callingController)
        {
            if (!callingController.GetComponent<Fighter>().CanAttack(gameObject))
            {
                return false;
            }

            //if (PlayerSettings.productName == "TooDeepToDig")
            //{
#if !ENABLE_INPUT_SYSTEM && !UNITY_Android
            if (Input.GetMouseButton(0)) //need to adapt later for cellphone
            {
                callingController.transform.GetComponent<Fighter>().Attack(gameObject);
            }
                
            //}
#endif
            
#if ENABLE_INPUT_SYSTEM || UNITY_Android
            if (Touchscreen.current.primaryTouch.isInProgress) //need to adapt later for cellphone
            {
                callingController.transform.GetComponent<Fighter>().Attack(gameObject);
            }
#endif
            return true;
        }

        public ShowOutline HasOutline(PlayerController callingController)
        {
            if (GetComponent<ShowOutline>() != null)
            {
                return GetComponent<ShowOutline>();
            }
            return null;
        }

        public void IsTarget()
        {
            if (token != null)
            {
                token.SetActive(true);
            }
        }

        public void IsNotTarget()
        {
            if (token != null)
            {
                token.SetActive(false);
            }
        }
        
        
    }
}