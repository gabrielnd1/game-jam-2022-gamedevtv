using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platform.Combat
{

    public class DisappearArrow : MonoBehaviour
    {
        public void Unload()
        {
            GetComponent<MeshRenderer>().enabled = false;
        }

        public void Reload()
        {
            GetComponent<MeshRenderer>().enabled = true;
        }
    }
}